package com.brief.API.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;


@Entity
@JsonIdentityInfo( // Pour éviter problème de récursivité
  generator = ObjectIdGenerators.PropertyGenerator.class, 
  property = "id")
public class CategoryModel {
    public CategoryModel(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    // En SQL pas besoin de connecter des deux côtés, mais en Java oui si on veut accéder facilement à chaque depuis l'autre
    // Une catégorie peut être sur plusieurs livres
    @OneToMany(mappedBy = "categoryModel") // nom de la propriété tel qu'il est dans l'autre entité 
    private Set<BookModel> bookModel;

    public Set<BookModel> getBookModel() {
        return bookModel;
    }

    public void setBookModel(Set<BookModel> bookModel) {
        this.bookModel = bookModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
