package com.brief.API.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Entity
@JsonIdentityInfo( // Pour éviter problème de récursivité
  generator = ObjectIdGenerators.PropertyGenerator.class, 
  property = "id")
public class BookModel {

    public BookModel(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    private String title;

    private String description;

    private Boolean available = true;

    // Plusieurs livres peuvent avoir une catégorie
    @ManyToOne
    private CategoryModel categoryModel;

    // Un livre peut posséder plusieurs auteurs
    @ManyToMany
    @JoinTable(
        name = "has_authors", 
        joinColumns = @JoinColumn(name = "book_id"), // pour choisir le nom de clé étrangère, optionnel
        inverseJoinColumns = @JoinColumn(name = "author_id")) // pour choisir le nom de clé étrangère, optionnel
    private Set<AuthorModel> authorModel;

    public Set<AuthorModel> getAuthorModel() {
        return authorModel;
    }

    public void setAuthorModel(Set<AuthorModel> authorModel) {
        this.authorModel = authorModel;
    }

    // Faire aussi get et set pour category car lié
    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}
