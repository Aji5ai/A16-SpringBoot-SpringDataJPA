package com.brief.API.model;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;

@Entity
@JsonIdentityInfo( // Pour éviter problème de récursivité
  generator = ObjectIdGenerators.PropertyGenerator.class,
  property = "id")
public class AuthorModel {
    public AuthorModel(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String firstName;

    // Un auteur peut écrire plusieurs livres
    @ManyToMany(mappedBy = "authorModel")
    private Set<BookModel> bookModel;

    public Set<BookModel> getBookModel() {
        return bookModel;
    }

    public void setBookModel(Set<BookModel> bookModel) {
        this.bookModel = bookModel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
}