package com.brief.API.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.brief.API.model.AuthorModel;

public interface AuthorRepository extends JpaRepository<AuthorModel, Long> {
     // Si on veut chercher que par name : 
    //  public List<AuthorModel> findByNameContainingIgnoreCase(String name);

     // Si on veut chercher à la fois sur name et firstname (et dans ce cas là pas besoin de la méthode précédente ligne 13) :
     @Query("SELECT a FROM AuthorModel a WHERE LOWER(a.name) LIKE LOWER(CONCAT('%', :keyword, '%')) OR LOWER(a.firstName) LIKE LOWER(CONCAT('%', :keyword, '%'))")
    List<AuthorModel> findByNameOrFirstNameContainingIgnoreCase(@Param("keyword") String keyword);
}
