package com.brief.API.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.brief.API.model.AuthorModel;
import com.brief.API.repository.AuthorRepository;

// versiona vec request mapping, autowired et response entoty (erreurs 404 etc)
@RestController
@RequestMapping("/authors")
public class AuthorController {
    @Autowired
    private AuthorRepository authorRepository;

    @GetMapping
    public List<AuthorModel> getAllAuthors() {
        return authorRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<AuthorModel> getAuthorById(@PathVariable Long id) {
        var optionalAuthor = authorRepository.findById(id);
        if (optionalAuthor.isPresent()) {
            var author = optionalAuthor.get();
            return ResponseEntity.ok(author);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteAuthorById(@PathVariable Long id) {
        var optionalAuthor = authorRepository.findById(id);
        if (optionalAuthor.isPresent()) {
            authorRepository.deleteById(id);
            return ResponseEntity.ok(true);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public AuthorModel postAuthor(@RequestBody AuthorModel authorModel) {
        return authorRepository.save(authorModel);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AuthorModel> updateAuthor(@PathVariable Long id, @RequestBody AuthorModel authorData) {

        var optionalAuthor = authorRepository.findById(id);
        if (optionalAuthor.isPresent()) {
            var existingAuthor = optionalAuthor.get();
            if (authorData.getName() != null && !authorData.getName().isEmpty()) {
                existingAuthor.setName(authorData.getName());
            }
            if (authorData.getFirstName() != null && !authorData.getFirstName().isEmpty()) {
                existingAuthor.setFirstName(authorData.getFirstName());
            }
            return ResponseEntity.ok(authorRepository.save(existingAuthor));
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // version pour chercher que pour name
    // @GetMapping("/search")
    // public List<AuthorModel> findByName(@RequestParam String name) {
    //     return authorRepository.findByNameContainingIgnoreCase(name);
    // }

    // version pour chercher à la fois sur name et sur firstname (ne pas garder version ligne 79 dans ce cas):
    @GetMapping("/search")
    public List<AuthorModel> findByNameOrFirstName(@RequestParam String name) {
        return authorRepository.findByNameOrFirstNameContainingIgnoreCase(name);
    }

}
